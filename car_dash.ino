#include "monitor.h"
#include "ecm.h"

// Board constants
#define MCP_INT 2
#define MCP_CS 10
#define CAN_BAUDRATE  CAN_500KBPS
#define MCP_OSC_SPEED MCP_16MHZ

// Global objects
MCP_CAN CAN0(MCP_CS);
//ECM ecu(&CAN0, MCP_INT);
Can_monitor can_monitor(&CAN0, MCP_INT);

// MCU setup function
void setup() {
  if (CAN0.begin(MCP_ANY, CAN_BAUDRATE, MCP_OSC_SPEED) == CAN_OK)
    Serial.println("MCP2515 Initialized Successfully!");
  else
    Serial.println("Error Initializing MCP2515...");

  CAN0.setMode(MCP_NORMAL);
}

// MCU main loop
void loop() {
  can_monitor.monitor();
}
