#ifndef ECU_H
#define ECU_H

#include "iso_tp.h"
#include "mcp_can.h"
#include "vector.h"

typedef stdard::Vector<uint8_t> BytesArray;

class ECU
{
    IsoTp   m_iso_tp;
    short   m_rxaddr, m_txaddr;
    struct  Message_t m_canmsg;
    uint8_t m_rxmessage[MAX_MSGBUF];
    short   m_rxmessage_length;

  protected:
    bool send_message(const uint8_t* message, short len);
    bool receive();
    uint8_t* receive_message(short& len);
    
  public:
    ECU(MCP_CAN* mcp_can, uint8_t mcp_int_pin, short rxAddr, short txAddr);
    ~ECU();

    bool send_iso(const BytesArray& message_in, BytesArray& message_out);
    bool startDiagnosticSession_aftersales();
};

#endif
