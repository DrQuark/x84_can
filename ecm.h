#ifndef ECM_H
#define ECM_H

#include "ecu.h"

class ECM : public ECU
{

public:
  ECM(MCP_CAN* mcp_can, uint8_t mcp_int_pin);
  ~ECM();
};

#endif
