#include "ecu.h"

ECU::ECU(MCP_CAN* mcp_can, uint8_t mcp_int_pin, short rxAddr, short txAddr) : m_iso_tp(mcp_can, mcp_int_pin)
{
  m_canmsg.tx_id   = m_txaddr;
  m_canmsg.rx_id   = m_rxaddr;
  m_canmsg.Buffer  = (uint8_t *)malloc(MAX_MSGBUF * sizeof(uint8_t));
}

ECU::~ECU()
{
  free(m_canmsg.Buffer);
}

bool 
ECU::startDiagnosticSession_aftersales()
{
  BytesArray sss(2), out;
  sss[0] = 0x10;
  sss[1] = 0xC0;
  return send_iso(sss, out);
}

bool
ECU::send_iso(const BytesArray& message_in, BytesArray& message_out)
{
  // Clear output message
  message_out.clear();
  
  if ( !send_message(&message_in[0], message_in.size()) )
    return false;

  short rcv_mess_len;
  uint8_t* rcv_message = receive_message(rcv_mess_len);
  
  if (rcv_message == NULL || !rcv_mess_len)
    return false;

  message_out.resize(rcv_mess_len, 0x00);
  for (int i = 0; i < rcv_mess_len; ++i){
    message_out[i] = rcv_message[i];
  }

  if (message_out[0] != (message_in[0] + 0x40) )
    return false;

  return true;
}

bool
ECU::send_message(const uint8_t* message, short len)
{
  if (len > MAX_MSGBUF)
    return false;
    
  m_canmsg.len = len;
  memcpy(m_canmsg.Buffer, message, len);
  
  return (m_iso_tp.send(&m_canmsg) == 0);
}

bool
ECU::receive()
{
  m_canmsg.len = 0;
  
  if (m_iso_tp.receive(&m_canmsg) != 0)
    return false;
    
  memcpy(m_rxmessage, m_canmsg.Buffer, m_canmsg.len);
  m_rxmessage_length = m_canmsg.len;
  
  if (m_canmsg.len == 0)
    return false;
    
  return true;
}

uint8_t* 
ECU::receive_message(short& len)
{
  len = 0;
  
  if ( !receive() )
    return NULL;
    
  len = m_rxmessage_length;
  return m_rxmessage;
}

