#ifndef MONITOR_H
#define MONITOR_H

#include "mcp_can.h"

enum ecu_ids {
  UCH_SYSTEM  = 0x035d,
  UCH_GENERAL = 0x060d,
  CLUSTER_BACKUP = 0x0715
};

class Can_monitor
{
    MCP_CAN*  m_can;
    uint8_t m_uch_general[8];
    uint8_t m_uch_system[8];
    uint8_t m_cluster_backup[8];
    uint8_t m_mcp_int;
  public:
    // if interrupt (INT) not used, mcp_int = 0
    Can_monitor(MCP_CAN* it, uint8_t mcp_int = 0);
    ~Can_monitor();

    void monitor();

    inline bool parking_light()
    {
      return m_uch_general[0] & 0b00000100;
    }

    inline bool low_beam_light()
    {
      return m_uch_general[0] & 0b00000010;
    }

    inline bool high_beam_light()
    {
      return m_uch_general[1] & 0b00001000;
    }

    inline bool fog_light()
    {
      return m_uch_general[1] & 0b00000001;
    }

    // external tempertaure in degrees centigrad
    inline int8_t external_temperature()
    {
      return m_uch_general[4] - 40;
    }

    // engine coolant in degrees centigrad
    inline int8_t engine_coolant_temperature()
    {
      return m_uch_general[5] - 40;
    }

    // reverse gear engaged > true
    inline bool reverse_gear()
    {
      return m_uch_general[6] & 0b00010000;
    }

    // driver door 
    // open>true 
    // close>false
    inline bool driver_door_open() {
      return ((m_uch_general[0] & 0b11111000) >> 3) & 0b00001;
    }

    inline bool passenger_door_open() {
      return ((m_uch_general[0] & 0b11111000) >> 3) & 0b00010;
    }

    // boot
    // open>true
    // close>false
    inline bool boot_open() {
      return ((m_uch_general[0] & 0b11111000) >> 3) & 0b10000;
    }

    // range
    // 0(low) <-> 8(max)
    inline uint8_t oil_level() {
      return (m_cluster_backup[3] & 0b00111100) >> 2;
    }

    // result in liters
    inline uint8_t gasoline_level() {
      return (m_cluster_backup[4] & 0b11111110) >> 1;
    }
};

#endif
