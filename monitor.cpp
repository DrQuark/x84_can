#include "monitor.h"

Can_monitor::Can_monitor(MCP_CAN* c, uint8_t mcp_int) : m_can(c)
{
  m_mcp_int = mcp_int;
}

Can_monitor::~Can_monitor()
{

}

void
Can_monitor::monitor()
{
  // Make a hard copy of the vehicle real time parameters
  // that can be used for later use
  uint8_t recv_frame[8];
  bool msgReceived;
  
  if (m_mcp_int){
    msgReceived = (!digitalRead(m_mcp_int));
  } else{
    msgReceived = (CAN_MSGAVAIL == m_can->checkReceive());
  }
  
  if (msgReceived) {
    uint32_t can_id;
    uint8_t len;
    if (m_can->readMsgBuf(&can_id, &len, recv_frame) == CAN_OK) {
      switch (can_id) {
        case UCH_GENERAL:
          memcpy(m_uch_general, recv_frame, 8);
          break;
        case UCH_SYSTEM:
          memcpy(m_uch_system, recv_frame, 8);
          break;
        case CLUSTER_BACKUP:
          memcpy(m_cluster_backup, recv_frame, 8);
          break;
        default:
          break;
      }
    }
  }
}

